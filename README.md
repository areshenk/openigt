# openigt

R package  containing the open source Iowa Gambling Task (IGT) data released by 
Steingroever et al. (2015) at https://osf.io/8t7rm/.

Package includes the complete dataset and a single function `openigt.query()`, 
which allows the user to select a subset of the data satisfying specified 
parameters.

The package can be installed a loaded using `devtools` as follows:
``` r
devtools::install_git('https://gitlab.com/fmriToolkit/spdm.git')
library(openigt)
```

The user can then specify the desired studies, payoff schemes, number of trials,
and age range, using
``` r
openigt.query(study = NULL, payoff = NULL, trials = NULL, age = NULL, trim = F) 
```
where `study` is a vector of study names, `payoff` is a vector of payoff schemes, 
`trials` is a numeric vector specifying the range of trials, and `age` is a numeric
vector specifying the range of desired ages. If any are NULL (defaults), then all
values will be returned. By default, the data are padded with NAs in the event of 
unequal trials lengths, so that if some subject have (e.g.) 100 trials, and others 
150, then the subjects with 100 trials will have their data padded to 150 with NAs.
If `trim = T`, then the function will only return the minimum of the trials number
(e.g. in the ebove example, only the first 100 trials will be returned for all 
subjects).

For example, if the user wants all studies using payoff schemes 1 and 2, only 
those studies with a mean age less than 40, and only the first 100 trials of those 
studies with between 100 and 150 trials, then they would specify
``` r
openigt.query(study = NULL, payoff = c(1,2), trials = c(100,150), age = c(0,40), trim = T) 
```

The function returns a named list with a data.frame `legend` containing subject
level information, and subject x trial matrices `win`, `loss`, `net`, and `choice`, containing
performance data.

See Steingroever et al. (2015) for more information about the dataset.

Steingroever, H., Fridberg, D. J., Horstmann, A., Kjome, K. L., Kumari, V., Lane, S. D., ... & Stout, J. (2015). 
Data from 617 healthy participants performing the Iowa gambling task: A" many labs" collaboration. 
Journal of Open Psychology Data, 3(1), 340-353.