openigt.query <- function(study = NULL, payoff = NULL,
                          trials = NULL, age = NULL, trim = F) {

    data(igtdata)
    if (is.null(study)) {
        study <- levels(igtdata$legend$Study)
    }
    if (is.null(payoff)) {
        payoff <- levels(igtdata$legend$Payoff)
    } else {
        payoff <- as.character(payoff)
    }
    if (is.null(trials)) {
        trials <- range(igtdata$legend$Trials, na.rm = T)
    }
    if (is.null(age)) {
        age.null <- T
        age <- range(igtdata$legend$Age, na.rm = T)
    } else {
        age.null <- F
    }

    # Get indices
    idx <- which(igtdata$legend$Study  %in% study &
                 igtdata$legend$Payoff %in% payoff &
                 igtdata$legend$Trials  >=  trials[1] &
                 igtdata$legend$Trials  <=  trials[2] &
                 ((igtdata$legend$Age     >=  age[1] &
                 igtdata$legend$Age     <=  age[2]) |
                     (age.null & is.na(igtdata$legend$Age))))

    # Truncate data, if requested
    if (trim) {
        n <- 1:min(igtdata$legend$Trials[idx])
    } else {
        n <- 1:max(igtdata$legend$Trials[idx])
    }

    data <- list(legend = igtdata$legend[idx,],
                 win    = igtdata$win[idx,n],
                 loss   = igtdata$loss[idx,n],
                 net    = igtdata$net[idx,n],
                 choice = igtdata$choice[idx,n])
    return(data)
}
